
1. RESTful CRUD in Node.js(express) and mySQL.
2. Local Authentication using passport


## Installation
*for newbies : Clone or download zip to your machine then hit this :

    cd nodeauth

then

    npm install

## Configuration (database)
/config/database.js

        host: 'localhost',
        user: 'root',
        password : '',
        port : 3306, //port mysql
        database:'db_nodeauth'	


	
You need to create a DB named 'db_nodeauth' or whatever you name it,  import db_nodeauth.sql


## Open your Browser
And type: localhost:3000
