// config/database.js
module.exports = {
    'connection': {
        host     : 'localhost',
        user     : 'root',
        password : '',
        database : 'db_nodeauth',
        debug    : false //set true if you wanna see debug logger
    }
};