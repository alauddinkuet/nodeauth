var express  = require('express'),
    path     = require('path'),
    bodyParser = require('body-parser'),
    app = express(),
    expressValidator = require('express-validator');
var connection  = require('express-myconnection'),
    mysql = require('mysql');


//For passport
var morgan = require('morgan'),
    cookieParser = require('cookie-parser'),
    session  = require('express-session');

var passport = require('passport');
var flash    = require('connect-flash');
require('./config/passport')(passport); // pass passport for configuration

app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
//end passport

var http = require('http');
var customers = require('./routes/customers');
//var user = require('./routes/user');
var utility = require('./lib/utility');

// required for passport
app.use(session({
    secret: 'vidyapathaisalwaysrunning',
    resave: true,
    saveUninitialized: true
} )); // session secret

app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session
//end passport

//load routes
var auth = require('./app/user')(app, passport); // load our routes and pass in our app and fully configured passport

/*Set EJS template Engine*/
app.set('views','./views');
app.set('view engine','ejs');


app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true })); //support x-www-form-urlencoded
app.use(bodyParser.json());
app.use(expressValidator());

/*MySql connection*/

var dbconfig = require('./config/database');
app.use(
    connection(mysql,dbconfig.connection,'request')
);

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


//app.get('/', routes.index);
app.get('/customers', utility.isLoggedIn, customers.list);
app.get('/customers/add', utility.isLoggedIn, customers.add);
app.post('/customers/add', utility.isLoggedIn, customers.save);
app.get('/customers/delete/:id', utility.isLoggedIn, customers.delete_customer);
app.get('/customers/edit/:id', utility.isLoggedIn, customers.edit);
app.post('/customers/edit/:id', utility.isLoggedIn,customers.save_edit);

/*
app.get('/users',user.get);
app.get('/user/:user_id',user.getById);
app.post('/saveuser',user.saveUser);
app.put('/updateuser/:user_id',user.updateUser);
app.delete('/deleteuser/:user_id',user.deleteUser);
*/
//RESTful route
//var router = express.Router();


/*------------------------------------------------------
*  This is router middleware,invoked everytime
*  we hit url /api and anything after /api
*  like /api/user , /api/user/7
*  we can use this for doing validation,authetication
*  for every route started with /api
--------------------------------------------------------*/
/*
router.use(function(req, res, next) {
    console.log(req.method, req.url);
    next();
});
*/
//now we need to apply our router here
//app.use('/api', router);

//start Server
var server = app.listen(3000,function(){
 console.log("Listening to port %s",server.address().port);
});